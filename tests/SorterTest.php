<?php

namespace Trip;

use PHPUnit\Framework\TestCase;
use Trip\Collections\Trips;
use Trip\Factories\Trip;

class SorterTest extends TestCase
{
    /** @var Sorter */
    private $sut;

    /** @var Trips|\PHPUnit_Framework_MockObject_MockObject */
    private $tripsCollectionMock;

    /** @var Trip|\PHPUnit_Framework_MockObject_MockObject */
    private $tripFactoryMock;

    /** @var string */
    private $data;

    public function setUp()
    {
        $this->tripsCollectionMock = $this->getMockBuilder(Trips::class)
            ->disableOriginalConstructor()
            ->setMethods([])
            ->getMock();

        $this->tripFactoryMock = $this->getMockBuilder(Trip::class)
            ->disableOriginalConstructor()
            ->setMethods([])
            ->getMock();

        $this->data = file_get_contents(__DIR__ . DIRECTORY_SEPARATOR . '..' . DIRECTORY_SEPARATOR . 'data' . DIRECTORY_SEPARATOR . 'cards.json');

        $this->sut = new Sorter($this->tripFactoryMock, $this->tripsCollectionMock);
    }

    /**
     * @expectedException \Trip\Exceptions\NoCardsFound
     */
    public function testExceptionIsThrownIfNoCardsProvided()
    {
        $this->sut->process('');
    }

    public function testEmptyArrayIsReturnedIfNothingProcessed()
    {
        $this->tripsCollectionMock->expects($this->once())->method('toArray')->will($this->returnValue([]));
        $this->assertEquals('[]', $this->sut->process($this->data));
    }
}
