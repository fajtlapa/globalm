<?php

namespace Trip;

use PHPUnit\Framework\TestCase;
use Trip\Collections\Trips;
use Trip\Factories\Trip;

class IntegrationTest extends TestCase
{
    const DATA_DIR = __DIR__ . DIRECTORY_SEPARATOR . '..' . DIRECTORY_SEPARATOR . '..' . DIRECTORY_SEPARATOR . 'data' . DIRECTORY_SEPARATOR;

    /** @var Sorter */
    private $sut;

    public function setUp()
    {
        $this->sut = new Sorter(new Trip(), new Trips());
    }

    /**
     * @dataProvider cardsDataProvider
     * @param string $input
     * @param string $expected
     */
    public function testScenarios($input, $expected)
    {
        $this->assertEquals($expected, $this->sut->process($input));
    }

    public function cardsDataProvider()
    {
        $expected = file_get_contents(self::DATA_DIR . 'expectedResult');

        return [
            [file_get_contents(self::DATA_DIR . 'cards.json'), $expected],
            [file_get_contents(self::DATA_DIR . 'unsortedCards.json'), $expected],
        ];
    }
}
