<?php

namespace Trip\Collections;

use PHPUnit\Framework\TestCase;
use Trip\Entities\Bus;

class TripsTest extends TestCase
{
    /** @var Trips */
    private $sut;

    public function setUp()
    {
        $this->sut = new Trips();
    }

    /**
     * @expectedException \Trip\Exceptions\NotFinishedTrip
     */
    public function testExceptionIsThrownIfTripIsIncomplete()
    {
        $this->sut->add(new Bus(["id" => "1502000000", "origin" => "Madrid", "destination" => "Barcelona"]));
        $this->sut->add(new Bus(["id" => "1503000000", "origin" => "Perth", "destination" => "Berlin"]));
        $this->sut->sort();
        $this->sut->toArray();
    }
}
