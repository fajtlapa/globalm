<?php

namespace Trip\Factories;

use PHPUnit\Framework\TestCase;
use Trip\Entities\Bus;
use Trip\Exceptions\UnknownTransportType;

class TripTest extends TestCase
{
    /** @var Trip */
    private $sut;

    public function setUp()
    {
        $this->sut = new Trip();
    }

    public function testExceptionIsThrownWhenNoTransportTypeIsSet()
    {
        try {
            $this->sut->fromArray(['transport_data' => ['transport_type' => 'foo']]);
        } catch (UnknownTransportType $ex) {
            $this->assertInstanceOf(UnknownTransportType::class, $ex);
        }
    }

    public function testCaseDoesNotMatter()
    {
        $this->assertInstanceOf(Bus::class, $this->sut->fromArray(['transport_data' => ['transport_type' => 'bUS']]));
    }
}
