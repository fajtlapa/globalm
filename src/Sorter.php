<?php
declare(strict_types=1);

namespace Trip;

use Trip\Exceptions\NoCardsFound;
use Trip\Factories\Trip as TripFactory;
use Trip\Collections\Trips as TripsCollection;
use Trip\Exceptions\NotFinishedTrip;

class Sorter
{
    /** @var TripsCollection */
    private $tripsCollection;

    /** @var TripFactory */
    private $tripsFactory;

    public function __construct(TripFactory $tripFactory, TripsCollection $tripCollection)
    {
        $this->tripsCollection = $tripCollection;
        $this->tripsFactory = $tripFactory;
    }

    /**
     * @param string $input
     *
     * @return string
     *
     * @throws NotFinishedTrip
     * @throws NoCardsFound
     */
    public function process(string $input): string
    {
        $arrayData = json_decode($input, true);

        if (0 === count($arrayData)) {
            throw new NoCardsFound('No cards found in input: ' . $input);
        }

        foreach ($arrayData as $entry) {
            $this->tripsCollection->add($this->tripsFactory->fromArray($entry));
        }
        $this->tripsCollection->sort();

        return json_encode($this->tripsCollection->toArray());
    }
}
