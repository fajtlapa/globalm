<?php
declare(strict_types=1);

namespace Trip\Entities;

class Plane extends Card
{
    const MSG_TRAVEL = 'From %s, take flight %s to %s. Gate %s, seat %s.';
    const MSG_COUNTER = 'Baggage drop at ticket counter %s.';
    const MSG_NO_COUNTER = 'Baggage will we automatically transferred from your last leg.';

    public function print(): string
    {
        $msg = self::MSG_TRAVEL . self::MSG_COUNTER;
        if ('' === $this->baggage_counter) {
            $msg = self::MSG_TRAVEL . self::MSG_NO_COUNTER;
        }
        return sprintf($msg, $this->origin, $this->line_number, $this->destination, $this->gate, $this->sit_assignment,  $this->baggage_counter);
    }
}
