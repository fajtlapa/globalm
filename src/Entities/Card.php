<?php
declare(strict_types=1);

namespace Trip\Entities;

/**
 * @property string line_number
 * @property string origin
 * @property string destination
 * @property string sit_assignment
 * @property string baggage_counter
 * @property string gate
 * @property string id
 */
abstract class Card
{
    /** @var string[] */
    protected $data;

    /**
     * @param string[] $data
     */
    public function __construct(array $data)
    {
        $this->data = $data;
    }

    /**
     * @param $name
     *
     * @return string
     */
    public function __get($name): string
    {
        if (!array_key_exists($name, $this->data)) {
            return '';
        }

        return $this->data[$name];
    }

    abstract public function print(): string;
}
