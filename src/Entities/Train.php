<?php
declare(strict_types=1);

namespace Trip\Entities;

class Train extends Card
{
    const MSG_TRAVEL = 'Take train %s from %s to %s.';
    const MSG_SIT = 'Sit in seat %s.';
    const MSG_NO_SIT = 'No seat assignment.';

    public function print(): string
    {
        $msg = self::MSG_TRAVEL . self::MSG_SIT;
        if ('' === $this->sit_assignment) {
            $msg = self::MSG_TRAVEL . self::MSG_NO_SIT;
        }
        return sprintf($msg, $this->line_number, $this->origin, $this->destination, $this->sit_assignment);
    }
}
