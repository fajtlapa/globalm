<?php
declare(strict_types=1);

namespace Trip\Collections;

use Trip\Entities\Card;
use Trip\Exceptions\NotFinishedTrip;

class Trips
{
    const MSG_DESTINATION_REACHED = 'You have arrived at your final destination.';

    /** @var array */
    private $cards = [];

    /**
     * Adds a trip to the collection
     *
     * @param Card $card
     */
    public function add(Card $card): void
    {
        $this->cards[$card->id] = $card;
    }

    /**
     * Removes a trip from the collection
     *
     * @param Card $card
     */
    public function remove(Card $card): void
    {
        unset($this->cards[$card->id]);
    }

    /**
     * Sorts trips by departure time
     */
    public function sort(): void
    {
        ksort($this->cards);
    }

    /**
     * Returns an list with hints regarding a trip
     *
     * @return array
     *
     * @throws NotFinishedTrip
     */
    public function toArray(): array
    {
        $result = [];
        $destination = '';
        foreach ($this->cards as $card) {
            if ('' !== $destination && $card->origin !== $destination) {
                throw new NotFinishedTrip('');
            }
            $destination = $card->destination;
            $result[] = $card->print();
        }
        $result[] = self::MSG_DESTINATION_REACHED;

        return $result;
    }
}
