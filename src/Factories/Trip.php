<?php
declare(strict_types=1);

namespace Trip\Factories;

use Trip\Entities\Bus;
use Trip\Entities\Card;
use Trip\Entities\Plane;
use Trip\Entities\Train;
use Trip\Exceptions\UnknownTransportType;

class Trip
{
    /**
     * @param array $data
     *
     * @return Card
     *
     * @throws UnknownTransportType
     */
    public function fromArray(array $data): Card
    {
        $data = array_merge($data, $data['transport_data']);
        unset($data['transport_data']);
        switch (mb_strtolower($data['transport_type'])) {
            case 'bus':
                return new Bus($data);
            case 'train':
                return new Train($data);
            case 'plane':
                return new Plane($data);
            default:
                throw new UnknownTransportType('Unknown type of transportation: ' . $data['transport_type']);
        }
    }
}
