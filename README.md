**Usage**

Simply use:

```
$sorter = new Sorter(new Trip(), new Trips());
$sorter->process($data);
```

, where `$data` is a JSON array containing (unordered) cards.

Please find an example input data in `.\data\cards.json` file.

**Test**

To run tests simply execute:

```
php vendor/bin/phpunit -c phpunit.xml
```

If you'd like to run only integration tests, please use `integration` tests suite as a parameter.

**Notes**

1. Whenever instructions were clear, initial instructions had a bug. Whenever a traveller goes back to one of the places
and then travels back, we run into problem, that all the sorting at this point would end in infinite loop
2. Whenever destination and origin would occur multiple times and their number would be even, it would be impossible to
find a correct starting/end point
3. If start and end point were the same, finding a start point would be impossible

With this in mind, timestamp (id) has been introduced. As all boarding passes will contain this value it simplified
logic and prevented bugs mentioned above.

**Final Note**

Assuming, that only finding a path by origin/destination would be an option, here's a possible algorithm:
 - find an origin that exists once only (starting point)
 - grab a destination assigned to this origin and find an origin with that destination (one level for loop - O(n) complexity)
 - throw an exception in case no origin found
 - iterate until we reach the size of an input array
 

**Improvements**

1. Given implementation does NOT validate neither filter input data. As of usage of magic `__get()` method at least one
of them should be implemented in future.
2. Many fields could be implemented as value objects (ie. destination), to make sure it's value stays unchanged and is
properly formatted
3. Tests skipped deep testing of entities as those are handled by integration tests already and contains limited amount
of business logic